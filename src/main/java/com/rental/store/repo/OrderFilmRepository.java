package com.rental.store.repo;

import com.rental.store.model.OrderFilm;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderFilmRepository extends CrudRepository<OrderFilm, Long> {

    List<OrderFilm> findByOrderId(Long orderId);

}