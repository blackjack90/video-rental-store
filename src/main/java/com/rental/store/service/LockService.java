package com.rental.store.service;

import java.util.concurrent.ConcurrentHashMap;

public class LockService {

    private ConcurrentHashMap<Long, Object> lockMap;

    public LockService() {
        lockMap = new ConcurrentHashMap<>();
    }

    public Object getCustomerLock(Long customerId) {
        return lockMap.computeIfAbsent(customerId, key -> new Object());
    }
}
