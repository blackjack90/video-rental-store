package com.rental.store.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class OrderFilm {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private RentOrder order;

    @ManyToOne
    private Film film;

    public OrderFilm(RentOrder order, Film film) {
        this.order = order;
        this.film = film;
    }
}
