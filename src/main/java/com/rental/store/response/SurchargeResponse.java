package com.rental.store.response;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SurchargeResponse {
    private String status;
    private Long orderId;
    private Integer surcharge;

}
